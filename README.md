# This is technically obsolete
thprac has this exact feature now making this SSG obsolete for any normal player. However, due to how thprac works, having it attached to your game process makes the game much harder to debug with a proper debugger. This isn't a deliberate anti-debug measure (if it was, I would've removed it since I'm in charge of thprac now), it's simply how thprac works. You can check the [source code](https://github.com/touhouworldcup/thprac/) to verify. x64dbg and WinDbg have mechanisms to deal with it, Visual Studio does not.

# The long awaited chapter practice for Legacy of Lunatic Kingdom
It does that and that only. If you wish to have spell practice too, use a different patch.

This project was meant to provide a more convenient way to practice specific chapters in any stage of Legacy of Lunatic Kingdom than playing through the entire stage, from a specific portion but not the chapter you want to practice or manually editing the game's memory.

## Usage
Enter the stage you want and pause. In the interface, select the stage you entered and select the chapter you want. Use the "Give up and retry" option in the pause menu to apply the changes. Note: the chapters are 0 indexed and midboss chapters count as chapters too. So the order will not be what you expect and don't think as the 6th chapter for example as chapter 6 but as chapter 5.
[EDIT: I'm kinda considering changing the chapters to being 1 indexed as I'm changing my mind towards the loading screen being chapter 0]

## Contributing
If you find the interface to be the worst interface one has ever seen, submit a pull request that makes the interface better. I am open for improvements. I will also gladly accept pull requests that change the way the chapters are named.

